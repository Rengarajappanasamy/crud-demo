package com.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
public class DoctorDetailNotValid extends RuntimeException{
 public DoctorDetailNotValid(String message) {
	super(message);
}
}

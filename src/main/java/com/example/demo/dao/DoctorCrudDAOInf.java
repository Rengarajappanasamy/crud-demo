package com.example.demo.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Doctor;

public interface DoctorCrudDAOInf extends CrudRepository<Doctor,Integer> {
	
	public Optional<Doctor> findByRegistrationNumber(String regNo);
	

}

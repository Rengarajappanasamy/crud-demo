package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Doctor;
import com.example.demo.service.DoctorCrudService;

@RestController
@RequestMapping("/doctor")
public class DoctorCrudController {

	@Autowired
	private DoctorCrudService service;
	
	@PostMapping
	private Doctor save(@RequestBody Doctor doctor) {
		
		 
		
	 return service.save(doctor);
	}
	
	  @GetMapping(value = "/{regNo}") private Doctor getDoctorDetail(@PathVariable
	  String regNo) { return service.getDoctor(regNo); }
	  
	  @GetMapping private List<Doctor> getAllDoctor(){ return service.getDoctors();
	  }
	  
	  @PutMapping(value = "/{regNo}") 
	  private Doctor update(@PathVariable(required  
	  = true) String regNo,@RequestBody Doctor doctor) {
		  System.out.println("inside method");
		  return 	  service.updateDoctorDetails(regNo,doctor);
	  
	  }
	  
	  @DeleteMapping(value = "/{regNo}")
	  private void delete(@PathVariable
	  String regNo) { 
		  service.delete(regNo);
	  
	  }
	 }

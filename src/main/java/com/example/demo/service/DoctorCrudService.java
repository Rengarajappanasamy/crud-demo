package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.exception.DoctorDetailNotValid;
import com.demo.exception.DoctorNotFoundException;
import com.example.demo.dao.DoctorCrudDAOInf;
import com.example.demo.model.Doctor;

@Service
public class DoctorCrudService {
  
	@Autowired
  private DoctorCrudDAOInf  doctorCrudDAO;
	
	public Doctor save(@Valid Doctor doctor) {
		try {
			doctor = doctorCrudDAO.save(doctor);
		}catch (Exception e) {
			throw new DoctorDetailNotValid("Provide valid customer Details");
		}
		return doctor;
	}

	public Doctor getDoctor(String regNo) {
		
		return doctorCrudDAO.findByRegistrationNumber(regNo).orElse(new Doctor());
	}

	public List<Doctor> getDoctors() {
		List<Doctor> doctorList  = (List<Doctor>) doctorCrudDAO.findAll();
	
		return doctorList;
	}

	public Doctor updateDoctorDetails(String regNo, Doctor doctor) {
		Optional<Doctor> doctorDetail=doctorCrudDAO.findByRegistrationNumber(regNo);
		if(doctorDetail.isPresent()) {
			
			Doctor doctor1 = doctorDetail.get();
			if(doctor.getDoctorName()!=null) {
				doctor1.setDoctorName(doctor.getDoctorName());
			}
			doctor1.setQualification(doctor.getQualification());
			doctor1.setSpeciality(doctor.getSpeciality());
		  System.out.println("doctor1"+doctor1);
			
			doctor = doctorCrudDAO.save(doctor1);
			
		}else {
			
			throw new DoctorNotFoundException("Doctor Details Not Found with given reg_no");
		}
		
		return doctor;
	}

	public void delete(String regNo) {
		Optional<Doctor> doctor = doctorCrudDAO.findByRegistrationNumber(regNo);
		if(doctor.isPresent()) {
			doctorCrudDAO.delete(doctor.get());
		}else {
			throw new DoctorNotFoundException("Doctor Details Not Found with given reg_no");
		}
		 
	}

}

package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.NaturalId;
import org.springframework.lang.Nullable;

@Entity
@Table(name = "doctor_details")

public class Doctor {
	@Id
	@GeneratedValue
	private int id;
	
	@NaturalId
	private String registrationNumber;
	
	private String doctorName;
	private String speciality;
	private String qualification;
	
	@NotNull
	@Column(name = "reg_no")
	public String getRegistrationNumber() {
		return registrationNumber;
	}
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@NotNull
	@Column(name = "doctorname")
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	@Column(name = "speciality")
	public String getSpeciality() {
		return speciality;
	}
	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}
	
	@Column(name = "qualification")
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	
	@Override
	public String toString() {
		return "Doctor [id=" + id + ", registrationNumber=" + registrationNumber + ", doctorName=" + doctorName
				+ ", speciality=" + speciality + ", qualification=" + qualification + "]";
	}
	
	
	
	

}
